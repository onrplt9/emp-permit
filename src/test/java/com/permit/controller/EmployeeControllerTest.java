package com.permit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.permit.model.dto.EmployeeDTO;
import com.permit.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@RunWith(SpringRunner.class)
@WebMvcTest({EmployeeController.class})
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    EmployeeService employeeService;

    @Test
    public void saveOrUpdateEmployee() throws Exception {

        EmployeeDTO employeeDTO = new EmployeeDTO("72b7d0e3-4851-4281-907c-7174d672f8d9","test","test","test@test.com",new Date());

        when(employeeService.createOrUpdateEmployee(any())).thenReturn(employeeDTO);

        mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/createOrUpdate")
                        .content(asJsonString(employeeDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }



    public static String asJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
