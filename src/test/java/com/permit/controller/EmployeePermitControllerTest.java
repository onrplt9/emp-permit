package com.permit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.permit.model.exception.ActionNotFoundException;
import com.permit.model.exception.PermitRangeNotValid;
import com.permit.model.permit.PermitAction;
import com.permit.model.permit.PermitRequest;
import com.permit.service.PermitService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.server.ResponseStatusException;

import java.text.SimpleDateFormat;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@RunWith(SpringRunner.class)
@WebMvcTest({PermitController.class})
public class EmployeePermitControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    PermitService permitService;

    private final SimpleDateFormat dateFormatter =new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void addPermit_200_validPermit() throws Exception {

        PermitRequest permitRequest = new PermitRequest("72b7d0e3-4851-4281-907c-7174d672f8d9"
                ,dateFormatter.parse("2021-08-26")
                ,dateFormatter.parse("2021-08-31"));

        when(permitService.grantPermit(any())).thenReturn(permitRequest);

        mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/permit/create")
                        .content(convertJsonString(permitRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    public void addPermit_400_notValidPermit() throws Exception {

        PermitRequest permitRequest = new PermitRequest("72b7d0e3-4851-4281-907c-7174d672f8d9"
                ,dateFormatter.parse("2021-08-31")
                ,dateFormatter.parse("2021-08-31"));

        when(permitService.grantPermit(any())).thenThrow(new PermitRangeNotValid());

        mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/permit/create")
                        .content(convertJsonString(permitRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void actionPermit_200_validAction() throws Exception {
        PermitRequest permitRequest = new PermitRequest("52b7d0e3-4851-4281-907c-7174d672f8d9"
                ,dateFormatter.parse("2021-08-20")
                ,dateFormatter.parse("2021-08-26"));

        PermitAction permitAction = new PermitAction("72b7d0e3-4851-4281-907c-7174d672f8d9"
                ,"APPROVED");

        when(permitService.takeAction(any())).thenReturn(permitRequest);

        mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/permit/action")
                        .content(convertJsonString(permitAction))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }

    @Test
    public void actionPermit_400_notValidAction() throws Exception {
        PermitRequest permitRequest = new PermitRequest("52b7d0e3-4851-4281-907c-7174d672f8d9"
                ,dateFormatter.parse("2021-08-20")
                ,dateFormatter.parse("2021-08-26"));

        PermitAction permitAction = new PermitAction("72b7d0e3-4851-4281-907c-7174d672f8d9"
                ,"NOT");

        when(permitService.takeAction(any())).thenThrow(new ActionNotFoundException());

        mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/permit/action")
                        .content(convertJsonString(permitAction))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());
    }



    public static String convertJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }
    }
}
