package com.permit.repository;

import com.permit.model.entity.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@ExtendWith(MockitoExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void initUseCase() {
        List<Employee> employees = Arrays.asList(
                new Employee("72b7d0e3-4851-4281-907c-7174d672f8d9","test","test","test@test.com",new Date())
        );
        employeeRepository.saveAll(employees);
    }

    @AfterEach
    public void destroyAll(){
        employeeRepository.deleteAll();
    }

    @Test
    void saveAll() {
        List<Employee> employees = Arrays.asList(
                new Employee("72b7d0e3-4851-4281-907c-7174d672f8d9","test","test","test@test.com",new Date()),
                new Employee("72b7d0e3-4851-4281-907c-7174d672f8d9","test-1","test-1","test-1@test.com",new Date())
        );
        Iterable<Employee> allCustomer = employeeRepository.saveAll(employees);

        AtomicInteger validIdFound = new AtomicInteger();
        allCustomer.forEach(employee -> {
            if(employee.getId()>0){
                validIdFound.getAndIncrement();
            }
        });

        assertThat(validIdFound.intValue()).isEqualTo(2);
    }

    @Test
    void findAll() {
        List<Employee> allEmployees = employeeRepository.findAll();
        assertThat(allEmployees.size()).isGreaterThanOrEqualTo(1);
    }
}
