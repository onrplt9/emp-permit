package com.permit.service;

import com.permit.model.entity.Employee;
import com.permit.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @BeforeEach
    void initUseCase() {
        employeeService = new EmployeeService(employeeRepository);
    }

    @Test
    public void insertEmployeeSuccess() {
        Employee employee = new Employee();
        employee.setFirstName("Onur");
        employee.setLastName("Polat");
        employee.setEmail("onur.polat@gmail.com");
        employee.setEmployeeId("72b7d0e3-4851-4281-907c-7174d672f8d9");
        employee.setStartDate(new Date());

        when(employeeRepository.save(any(Employee.class))).thenReturn(employee);


        Employee savedEmployee = employeeRepository.save(employee);
        assertThat(savedEmployee.getEmployeeId()).isNotNull();
    }

    @Test
    public void deleteEmployeeSuccess() {
        String employeeId="72b7d0e3-4851-4281-907c-7174d672f8d9";

        employeeService.deleteEmployee(employeeId);

        verify(employeeRepository, times(1)).deleteByEmployeeId(eq(employeeId));
    }

}
