package com.permit.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.permit.model.holiday.Holiday;
import com.permit.util.HolidayStaticRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@Component
public class PostStartupHandler {

    @Autowired
    ResourceLoader resourceLoader;

    @PostConstruct
    public void init() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        try (FileReader fileReader = new FileReader("src/main/resources/holiday-mock.json");
             BufferedReader reader = new BufferedReader(fileReader)) {
            String jsonContent = reader.lines()
                    .collect(Collectors.joining(System.lineSeparator()));

            //conver json string to Holiday object
            List<Holiday> holiday = mapper.readValue(jsonContent, new TypeReference<ArrayList<Holiday>>() {});

            // keep holiday arraylist at regitry class static variable and access it  from other blocks.
            HolidayStaticRegistry.getInstance().addHolidaytoStaticList(holiday);
        }

    }
}
