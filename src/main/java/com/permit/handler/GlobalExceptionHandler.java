package com.permit.handler;

import com.permit.model.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String NOT_VALID_PERMIT = "Exception.notValidPermit";
    private static final String NOT_VALID_PERMIT_RANGE = "Exception.permitDateRangeNotValid";
    private static final String NOT_VALID_PERMIT_DAY = "Exception.permitWeekendOrHoliday";
    private static final String NOT_VALID_PERMIT_ACTION = "Exception.actionNotValid";

    @Autowired
    private  MessageSource messageSource;

    @ExceptionHandler(PermitRequestNotValid.class)
    public ResponseEntity<Object> handlePermitRequestNotValidException(
             Locale locale
    ){
        String message = messageSource.getMessage(NOT_VALID_PERMIT, null, locale);
        return new ResponseEntity<>(new ExceptionMessage(message),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PermitRangeNotValid.class)
    public ResponseEntity<Object> handlePermitRequestRangeNotValidException(
            Locale locale
    ){
        String message = messageSource.getMessage(NOT_VALID_PERMIT_RANGE, null, locale);
        return new ResponseEntity<>(new ExceptionMessage(message),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PermitRequestWeekendException.class)
    public ResponseEntity<Object> handlePermitRequestWeekendException(
            Locale locale
    ){
        String message = messageSource.getMessage(NOT_VALID_PERMIT_DAY, null, locale);
        return new ResponseEntity<>(new ExceptionMessage(message),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ActionNotFoundException.class)
    public ResponseEntity<Object> handleActionNotFoundException(
            Locale locale
    ){
        String message = messageSource.getMessage(NOT_VALID_PERMIT_ACTION, null, locale);
        return new ResponseEntity<>(new ExceptionMessage(message),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleRuntimeException(
            RuntimeException runtimeException
    ){
        return new ResponseEntity<>(new ExceptionMessage(runtimeException.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
