package com.permit.repository;

import com.permit.model.entity.EmployeePermit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@Repository
public interface EmployeePermitRepository extends JpaRepository<EmployeePermit,Long> {

    @Query(value = "select coalesce (sum(permit_day), 0) from employee_permit where employee_id = :employeeId  and status != 'REJECTED' "
            ,nativeQuery = true)
    Integer getSumOfValidDay(@Param("employeeId") String employeeId);


    @Query(value = "select count(*) from employee_permit ep  where employee_id  = :employeeId " +
            "and end_date >= :endDate and start_date <= :startDate"
            ,nativeQuery = true)
    Integer checkForDateRange(@Param("employeeId") String employeeId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);

    EmployeePermit getByPermitId(String permitId);
}
