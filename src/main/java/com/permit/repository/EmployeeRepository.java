package com.permit.repository;

import com.permit.model.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    void deleteByEmployeeId(String employeeId);
    Employee getByEmployeeId(String employeeId);
}
