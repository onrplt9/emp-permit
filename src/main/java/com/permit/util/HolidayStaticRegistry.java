package com.permit.util;

import com.permit.model.holiday.Holiday;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class HolidayStaticRegistry {
    private static final HolidayStaticRegistry instance = new HolidayStaticRegistry();

    public static HolidayStaticRegistry getInstance() {
        return instance;
    }

    private static ArrayList<Holiday> holidays;

    private HolidayStaticRegistry() {
        holidays  = new ArrayList<>();
    }

    public boolean addHolidaytoStaticList(List<Holiday> holidayList){
        holidays.addAll(holidayList);
        return true;
    }

    public boolean checkIfInputIsHoliday(String date) {
        AtomicBoolean returnValue = new AtomicBoolean(false);

        holidays.forEach(holiday -> {
            if(holiday.getPublic() && holiday.getDate().equals(date)) {
                returnValue.set(true);
            }
        });

        return returnValue.get();
    }
}
