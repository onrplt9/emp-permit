package com.permit.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
public class PermitUtil {

    public static int getPermitDayBetweenDateRange (Date startDate, Date endDate) {
        int count = 0;

        LocalDate start = startDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate end = endDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate next = start.minusDays(1);
        while ((next = next.plusDays(1)).isBefore(end.plusDays(1))) {
            if(!isWeekend(next) && !isHoliday(next)) {
                count++;
            }
        }

        return count;
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek d = date.getDayOfWeek();
        return d == DayOfWeek.SATURDAY || d == DayOfWeek.SUNDAY;
    }

    public static boolean isHoliday(LocalDate date) {
        String formattedDate = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        if(HolidayStaticRegistry.getInstance().checkIfInputIsHoliday(formattedDate)) {
            return true;
        } else {
            return false;
        }
    }
}
