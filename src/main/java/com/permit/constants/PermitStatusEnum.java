package com.permit.constants;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public enum PermitStatusEnum {
    AWAITING_APPROVAL,
    APPROVED,
    REJECTED;

    public static PermitStatusEnum checkIfExists(String action) {
        for (PermitStatusEnum permitStatus : values()) {
            if (permitStatus.name().equalsIgnoreCase(action)) {
                return permitStatus;
            }
        }
        return null;
    }
}
