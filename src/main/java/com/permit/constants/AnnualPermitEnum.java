package com.permit.constants;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public enum AnnualPermitEnum {

        BEGINNER(5L,0L),
        ONE_TO_FIVE(15L,5L),
        FIVE_TO_TEN(18L,10L),
        TEN_PLUS(24L,Long.MAX_VALUE);

        AnnualPermitEnum(Long deservedPermit,Long operatingTime) {
            this.deservedPermit = deservedPermit;
            this.operatingTime = operatingTime;
        }

        private Long deservedPermit;
        private Long operatingTime;

        public Long getDeservedPermit() {
            return deservedPermit;
        }

        public static AnnualPermitEnum getAnnualPermit(Long operatingTime) {
            AnnualPermitEnum found = null;

            for (AnnualPermitEnum annualPermit : values()) {
                if (annualPermit.operatingTime >= operatingTime){
                    found = annualPermit;
                    break;
                }
            }

            return found;
        }

}
