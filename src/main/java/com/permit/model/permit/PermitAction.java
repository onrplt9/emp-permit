package com.permit.model.permit;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class PermitAction {
    private String permitId;
    private String action;

    public PermitAction() {
    }

    public PermitAction(String permitId, String action) {
        this.permitId = permitId;
        this.action = action;
    }

    public String getPermitId() {
        return permitId;
    }

    public void setPermitId(String permitId) {
        this.permitId = permitId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
