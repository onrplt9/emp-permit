package com.permit.model.dto;

import java.util.Date;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class EmployeePermitDTO {
    private String employeeId;
    private Integer permitDay;
    private String status;
    private Date startDate;
    private Date endDate;

    public EmployeePermitDTO(String employeeId, Integer permitDay, String status, Date startDate, Date endDate) {
        this.employeeId = employeeId;
        this.permitDay = permitDay;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public EmployeePermitDTO() {
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getPermitDay() {
        return permitDay;
    }

    public void setPermitDay(Integer permitDay) {
        this.permitDay = permitDay;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
