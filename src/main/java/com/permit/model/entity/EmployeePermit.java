package com.permit.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.permit.constants.PermitStatusEnum;

import javax.persistence.*;
import java.util.Date;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@Entity
@Table(name = "employee_permit")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeePermit {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="employee_id",referencedColumnName = "employee_id", nullable=false)
    private Employee employee;

    @Column(name = "permit_day")
    private Integer permitDay;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "permit_id")
    private String permitId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PermitStatusEnum status;

    public EmployeePermit() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Integer getPermitDay() {
        return permitDay;
    }

    public void setPermitDay(Integer permitDay) {
        this.permitDay = permitDay;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public PermitStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PermitStatusEnum status) {
        this.status = status;
    }

    public String getPermitId() {
        return permitId;
    }

    public void setPermitId(String permitId) {
        this.permitId = permitId;
    }

    @PrePersist
    protected void onCreate() {
        setPermitId(java.util.UUID.randomUUID().toString());
    }
}
