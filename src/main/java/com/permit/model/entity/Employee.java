package com.permit.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "start_date")
    private Date startDate = new Date();

    //in order to prevent infinite recursion
    @JsonManagedReference
    @OneToMany(mappedBy="employee")
    private Set<EmployeePermit> employeePermits;

    public Employee() {
    }

    public Employee(String employeeId, String firstName, String lastName, String email, Date startDate) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.startDate = startDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Set<EmployeePermit> getEmployeePermits() {
        return employeePermits;
    }

    public void setEmployeePermits(Set<EmployeePermit> employeePermits) {
        this.employeePermits = employeePermits;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @PrePersist
    protected void onCreate() {
        setEmployeeId(java.util.UUID.randomUUID().toString());
    }
}
