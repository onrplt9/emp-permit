package com.permit.model.holiday;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class Holiday {

    @JsonProperty("name")
    private String name;
    @JsonProperty("date")
    private String date;
    @JsonProperty("observed")
    private String observed;
    @JsonProperty("public")
    private Boolean _public;
    @JsonProperty("country")
    private String country;
    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("weekday")
    private Weekday weekday;

    public Holiday() {
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("observed")
    public String getObserved() {
        return observed;
    }

    @JsonProperty("observed")
    public void setObserved(String observed) {
        this.observed = observed;
    }

    @JsonProperty("public")
    public Boolean getPublic() {
        return _public;
    }

    @JsonProperty("public")
    public void setPublic(Boolean _public) {
        this._public = _public;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("weekday")
    public Weekday getWeekday() {
        return weekday;
    }

    @JsonProperty("weekday")
    public void setWeekday(Weekday weekday) {
        this.weekday = weekday;
    }
}
