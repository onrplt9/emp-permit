package com.permit.model.holiday;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weekday {

    @JsonProperty("observed")
    private Observed observed;

    @JsonProperty("observed")
    public Observed getObserved() {
        return observed;
    }

    @JsonProperty("observed")
    public void setObserved(Observed observed) {
        this.observed = observed;
    }

}
