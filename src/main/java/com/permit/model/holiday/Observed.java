package com.permit.model.holiday;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class Observed {

    @JsonProperty("name")
    private String name;
    @JsonProperty("numeric")
    private String numeric;

    public Observed() {
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("numeric")
    public String getNumeric() {
        return numeric;
    }

    @JsonProperty("numeric")
    public void setNumeric(String numeric) {
        this.numeric = numeric;
    }

}