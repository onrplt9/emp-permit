package com.permit.model.exception;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class ExceptionMessage {
    private  boolean success;
    private String message;

    public ExceptionMessage(String message) {
        this.message = message;
        this.success = false;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }
}
