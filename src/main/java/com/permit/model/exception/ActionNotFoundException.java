package com.permit.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class ActionNotFoundException extends ResponseStatusException {

    public ActionNotFoundException() {
        super(HttpStatus.BAD_REQUEST,"Action is not valid");
    }
}
