package com.permit.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
public class PermitRequestNotValid  extends ResponseStatusException {

    public PermitRequestNotValid() {
        super(HttpStatus.BAD_REQUEST,"Permit Request is not valid!");
    }

}
