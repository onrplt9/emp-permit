package com.permit.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
public class PermitRangeNotValid extends ResponseStatusException {

    public PermitRangeNotValid() {
        super(HttpStatus.BAD_REQUEST,"Permit range not valid");
    }
}
