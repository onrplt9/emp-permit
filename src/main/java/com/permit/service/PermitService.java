package com.permit.service;

import com.permit.constants.AnnualPermitEnum;
import com.permit.constants.PermitStatusEnum;
import com.permit.model.entity.Employee;
import com.permit.model.entity.EmployeePermit;
import com.permit.model.exception.ActionNotFoundException;
import com.permit.model.exception.PermitRangeNotValid;
import com.permit.model.exception.PermitRequestNotValid;
import com.permit.model.exception.PermitRequestWeekendException;
import com.permit.model.permit.PermitAction;
import com.permit.model.permit.PermitRequest;
import com.permit.repository.EmployeePermitRepository;
import com.permit.repository.EmployeeRepository;
import com.permit.util.PermitUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

/**
 * created by Onur Polat
 * created date 02/06/2021
 */
@Service
public class PermitService {

    @Autowired
    private EmployeePermitRepository employeePermitRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    public PermitRequest grantPermit(PermitRequest permitRequest) {
        //calculate the "real" number of days  between  requested date range ( exclude weekend and national holidays if necessary)
        Date startDate = permitRequest.getStartDate();
        Date endDate = permitRequest.getEndDate();

        int dayCount = PermitUtil.getPermitDayBetweenDateRange(startDate,endDate);

        if(dayCount != 0 ) {
           EmployeePermit employeePermit =  checkIfDateRangeIsValid(permitRequest.getEmployeeId(),startDate,endDate,dayCount);
           return convertEntitytoDTO(employeePermit);
        } else {
            throw new PermitRequestWeekendException();
        }
    }

    public EmployeePermit checkIfRequestedPermitIsValid(Employee employee,int dayCount,Date startDate,Date endDate) {
        // get employee permit for the employee him/herself and place the permit condition as  != "Rejected"
        Integer usedPermitCount = null;
        try {
            usedPermitCount  = this.employeePermitRepository.getSumOfValidDay(employee.getEmployeeId());
        } catch (Exception e) {
            e.printStackTrace();
        }



        // check if  (deserved annual leave  - used permit)  greater than requested day
        LocalDate startLocalDate = employee.getStartDate().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate now = new Date().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();


        Period period = Period.between(startLocalDate, now);

        int years = period.getYears();

        Long deservedAnnualLeave = AnnualPermitEnum.getAnnualPermit((long) years).getDeservedPermit();

        if((deservedAnnualLeave - usedPermitCount ) > dayCount ) {
            EmployeePermit employeePermit = new EmployeePermit();
            employeePermit.setPermitDay(dayCount);
            employeePermit.setEmployee(employee);
            employeePermit.setStartDate(startDate);
            employeePermit.setEndDate(endDate);
            employeePermit.setStatus(PermitStatusEnum.AWAITING_APPROVAL);

            try {
                return this.employeePermitRepository.save(employeePermit);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
            }
        } else {
            throw new PermitRequestNotValid();
        }

    }

    public EmployeePermit checkIfDateRangeIsValid(String employeeId, Date startDate, Date endDate, int dayCount) {
        Integer rowCount = null;

        try {
            rowCount = this.employeePermitRepository.checkForDateRange(employeeId,startDate,endDate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }

        if(rowCount == 0) {
            Employee employee = null;

            try {
                employee = this.employeeRepository.getByEmployeeId(employeeId);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //check if requested permit date is valid
            return checkIfRequestedPermitIsValid(employee, dayCount,startDate,endDate);
        } else {
            throw new PermitRangeNotValid();
        }
    }

    public PermitRequest takeAction(PermitAction permitAction) {
        EmployeePermit employeePermit = null;

        try {
            employeePermit =  this.employeePermitRepository.getByPermitId(permitAction.getPermitId());

            String action = permitAction.getAction();
            PermitStatusEnum  permitStatus = PermitStatusEnum.checkIfExists(action);

            if (permitStatus != null ) {
                employeePermit.setStatus(permitStatus);
                employeePermit = this.employeePermitRepository.save(employeePermit);
            } else {
                throw new ActionNotFoundException();
            }

        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }

        return convertEntitytoDTO(employeePermit);
    }

    public PermitRequest convertEntitytoDTO(EmployeePermit employeePermit){
        return modelMapper.map(employeePermit, PermitRequest.class);
    }

}
