package com.permit.service;

import com.permit.model.dto.EmployeeDTO;
import com.permit.model.entity.Employee;
import com.permit.repository.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    //map to entity and vice-versa in order not to expose primary id of the database (use uuid)
    public EmployeeDTO createOrUpdateEmployee (EmployeeDTO employeeDTO) {
        Employee employee = null;

        try {
            Employee convertedFromEmployeeDTO = convertDTOtoEntity(employeeDTO);

            employee = this.employeeRepository.getByEmployeeId(employeeDTO.getEmployeeId());

            if(employee != null ){
                convertedFromEmployeeDTO.setId(employee.getId());
            }
            employee = this.employeeRepository.save(convertedFromEmployeeDTO);
        } catch ( Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }

        return convertEntitytoDTO(employee);
    }

    public void deleteEmployee (String id) {
        try {
            this.employeeRepository.deleteByEmployeeId(id);
        } catch ( Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }
    }

    public EmployeeDTO getEmployee (String id) {
        Employee employee = null;

        try {
            employee = this.employeeRepository.getByEmployeeId(id);
        } catch ( Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }

        return convertEntitytoDTO(employee);
    }

    public Employee convertDTOtoEntity(EmployeeDTO employeeDTO){
        return modelMapper.map(employeeDTO, Employee.class);
    }

    public EmployeeDTO convertEntitytoDTO(Employee employee){
        return modelMapper.map(employee, EmployeeDTO.class);
    }
}


