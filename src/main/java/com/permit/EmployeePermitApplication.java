package com.permit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application.properties")
public class EmployeePermitApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeePermitApplication.class, args);
	}

}
