package com.permit.controller;

import com.permit.model.dto.EmployeeDTO;
import com.permit.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(path="/createOrUpdate",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeDTO createOrUpdateEmployee(@RequestBody EmployeeDTO employeeDTO){
        return this.employeeService.createOrUpdateEmployee(employeeDTO);
    }

    @DeleteMapping(path = "/delete/{employeeId}")
    public ResponseEntity<?> deleteEmployee(@PathVariable String employeeId){
        this.employeeService.deleteEmployee(employeeId);
        return new ResponseEntity<>(employeeId, HttpStatus.OK);
    }

    @GetMapping(path = "/{employeeId}",produces = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeDTO getEmployee(@PathVariable String employeeId) {
        return this.employeeService.getEmployee(employeeId);
    }

}
