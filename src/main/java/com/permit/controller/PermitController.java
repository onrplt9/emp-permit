package com.permit.controller;

import com.permit.model.permit.PermitAction;
import com.permit.model.permit.PermitRequest;
import com.permit.service.PermitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * created by Onur Polat
 * created date 01/06/2021
 */
@RestController
@RequestMapping(value = "/employee/permit")
public class PermitController {

    @Autowired
    private PermitService permitService;

    @PostMapping(path="/create",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public PermitRequest createPermit(@RequestBody PermitRequest permitRequest){
        return this.permitService.grantPermit(permitRequest);
    }

    @PostMapping(path="/action",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public PermitRequest permitAction(@RequestBody PermitAction permitAction){
        return this.permitService.takeAction(permitAction);
    }


}
