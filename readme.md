# Employee Annual Leave Management

### Prerequisite

* Postgresql latest version.

* Java 1.8

### Serve

Embedded Server

`http://localhost:8080`

Swagger Console

`http://localhost:8080/swagger-ui.html`
